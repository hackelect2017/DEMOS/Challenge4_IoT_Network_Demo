/*
 
 #### USE : 
 # Files are storred under ./data 
 # Start ES AWS Proxy - seperate shell
 
 cd /opt/hackelect
 ./node/bin/node  ./esSearchProxy.js
  
 */

// --- CURL example:
// curl -XGET "http://127.0.0.1:9200/smartnim-*/_search" -H 'Content-Type: application/json' -d'{"from":0,"size":10000,"sort":[{"@timestamp":"asc"}],"query":{"bool":{"must":[{"match":{"_type":"smartnim"}},{"range":{"@timestamp":{"gte":"2017-09-28T05:00:00","lt":"2017-09-28T06:00:00"}}}]}}}'



// -----------------------------------------------------------------------------------------------------------
// --- SmartNIM set dates - DO NOT configure more than 1 day !!!
// -----------------------------------------------------------------------------------------------------------
var dateStart = "2017-10-01T00:00:00"
var dateEnd = "2017-10-01T23:59:59"

var properties = {
    esMaxHits: 10000,
    esProxySearchUrl: "http://127.0.0.1:9200/",
    httpsRestTimeoutSec: 10,
    smartNimEsIndex: "smartnim-*",
    supplementarySourcesIndex: "supplementarysources-*"
};

// -----------------------------------------------------------------------------------------------------------
// ---
// -----------------------------------------------------------------------------------------------------------
var fs = require('fs');
var moment = require('moment');
var request = require("request")

// -----------------------------------------------------------------------------------------------------------
// --- ES REST request
// -----------------------------------------------------------------------------------------------------------
function esDslSearchQuery(index, query, cbf) {
    var restRequestObject = {
        url: properties.esProxySearchUrl + index + "/_search",
        method: "GET",
        json: query,
        timeout: properties.httpsRestTimeoutSec * 1000
    };
    request(restRequestObject, function (error, response, body) {
        // --- request NOK 
        if (error) {
            var err = {
                error: "request ERROR",
                httpRequest: restRequestObject,
                httpErrorMessage: error
            };
            //console.log(err);
            cbf(err, null);
        }
        // --- request OK 
        else {
            var res = body
            //console.log(res);
            cbf(null, res)
        }
    })
}

// -----------------------------------------------------------------------------------------------------------
// --- Elastic Search  esDslQuerySmartNim 
// -----------------------------------------------------------------------------------------------------------
var esDslQueryGetSmartNim = {
    "from": 0, "size": properties.esMaxHits,
    "sort": [
        {"@timestamp": "asc"}
    ],
    "query": {
        "bool": {
            "must": [
                {"match": {"_type": "smartnim"}},
                //  {"match": {"testType": "ipSla"}},
                {"range": {
                        "@timestamp": {
                            "gte": dateStart, // "2017-09-01T00:00:00",
                            "lt": dateEnd
                        }
                    }
                }
            ]
        }
    }
}

esDslSearchQuery(properties.smartNimEsIndex, esDslQueryGetSmartNim, function (err, res) {
    if (err) {
        console.log("esDslQuerySmartNim ERROR: ", err)
    } else {
        console.log("esDslQuery SmartNIM OK - total hits: ", res.hits.total)
        res = res.hits.hits.map(function (d) {
            return d._source
        })
        saveSmartNimJson(res)
    }
})

function saveSmartNimJson(smartNimJson) {
    var smartNimJsonStringify = JSON.stringify(smartNimJson, null, 1)
    var smartNimJsonFilename = "smartNim" + dateStart + "---" + dateEnd + ".json"
    fs.writeFile( smartNimJsonFilename, smartNimJsonStringify, function (err) {
        console.log("smartNimJson saved : " + smartNimJsonFilename)
    });
}

// -----------------------------------------------------------------------------------------------------------
// --- Elastic Search  esDslQuerySupplementarySources 
// -----------------------------------------------------------------------------------------------------------
var esDslQueryGetAllSupplementarySources = {
    "from": 0, "size": properties.esMaxHits,
    "sort": [
        {"@timestamp": "asc"}
    ],
    "query": {
        "bool": {
            "must": [
                // {"match": {"_type": "smartnim"}},
                // {"match": {"testType": "ipSla"}},
                {"range": {
                        "@timestamp": {
                            "gte": dateStart, // "2017-09-01T00:00:00",
                            "lt": dateEnd
                        }
                    }
                }
            ]
        }
    }
}

esDslSearchQuery(properties.supplementarySourcesIndex, esDslQueryGetAllSupplementarySources, function (err, res) {
    if (err) {
        console.log("esDslQuerySmartNim ERROR: ", err)
    } else {
        console.log("esDslQuery Supplementary Sources OK - total hits: ", res.hits.total)
        res = res.hits.hits.map(function (d) {
            return d._source
        })
        res.map(function (imageObj) {
             decodeSaveImage(imageObj)
        })
    }
})

function decodeSaveImage(imageObj) {
    var imageBase64Decoded = new Buffer(imageObj.imageBase64Encoded, 'base64')
    var imageName = imageObj.imageId + "_" + moment(imageObj.timestamp).format('YYYYMMDDTHH:mm:ss') + ".jpg"
    fs.writeFile( imageName, imageBase64Decoded, function (err) {
        console.log("image saved : " + imageName)
    });
}
