/*
 #### USE :  
 cd /opt/hackelect
 ./node/bin/node  ./hackelect-4iot.js

 # Files are storred under ./data 
 
 */

// -----------------------------------------------------------------------------------------------------------
// --- SmartNIM set dates - DO NOT configure more than 1 day !!!
// -----------------------------------------------------------------------------------------------------------
var dateStart = "2017-09-28T05:00:00"
var dateEnd =   "2017-09-28T08:00:00"

var properties = {
    "esServer": {
        "host": "elastic:9200",
        "type": "smartnim"
    },
    "esServerAws": {
        "accessKeyId": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
        "secretAccessKey": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
        "service": "es",
        "region": "eu-central-1",  
        "host": "search-hackelect-s234lvnv2z6tcjpor4immsn2bq.eu-central-1.es.amazonaws.com"
    },
    "smartNimEsIndex":"smartnim-*",
    "supplementarySourcesIndex": "supplementarysources-*"
};


// -----------------------------------------------------------------------------------------------------------
// --- ES Client Initialize  
// -----------------------------------------------------------------------------------------------------------
var fs = require('fs');
var elasticsearchAvs = require('aws-elasticsearch-client');
var elasticsearch = require('elasticsearch');
var moment = require('moment');

var esServerAwsConnect = properties.esServerAws
var esServerConnect = properties.esServer

var esClient =  elasticsearchAvs(esServerAwsConnect);
//var esClient = new elasticsearch.Client(esServerConnect);


// -----------------------------------------------------------------------------------------------------------
// --- Elastic Search 
// -----------------------------------------------------------------------------------------------------------
function storeToElastic(obj, cbf) {
    var message = JSON.parse(JSON.stringify(obj))
    delete message.esIndex

    var elasticsearchObjectTmp = {
        "index": obj.esIndex, //"samrtnim-" + moment(new Date()).format('YYYYMMDD'),
        "type": properties.esServer.type,
        //"id": message.testId + message.testTime,
        "body": message
    }

    esClient.index(elasticsearchObjectTmp, function (err, resp, status) {
        if (err) {
            console.log("elasticsearch insert ERROR: ", err, elasticsearchObjectTmp);
            cbf(err, null)
        } else {
            console.log("elasticsearch insert OK");
            cbf(null, resp)
        }
    });
}

function esDslQuery(obj, cbf) {
    esClient.search(obj, function (err, res) {
        if (err) {
            cbf(err, null)
        } else {
            cbf(null, res.hits.hits)
        }
    });
}

// -----------------------------------------------------------------------------------------------------------
// --- Elastic Search  esDslQuerySmartNim 
// -----------------------------------------------------------------------------------------------------------
var esDslQueryGetSmartNim = {
    "from": 0, "size": 10000,
    "sort": [
        {"@timestamp": "asc"}
    ],
    "query": {
        "bool": {
            "must": [
                {"match": {"_type": "smartnim"}},
                //  {"match": {"testType": "ipSla"}},
                {"range": {
                        "@timestamp": {
                            "gte": dateStart, // "2017-09-01T00:00:00",
                            "lt": dateEnd
                        }
                    }
                }
            ]
        }
    }
}

esDslQuerySmartNim(esDslQueryGetSmartNim, function (err, res) {
    if (err) {
        console.log("esDslQuerySmartNim ERROR: ", err)
    } else {
        saveSmartNimJson(res)
    }
})

function esDslQuerySmartNim(obj, cbf) {

    var dslQuery = {
        index: properties.smartNimEsIndex,
        body: obj
    }

    esClient.search(dslQuery, function (err, res) {
        if (err) {
            cbf(err, null)
        } else {
            cbf(null, res.hits.hits.map(function (d) {
                return d._source
            }))
        }
    });
}

function saveSmartNimJson(smartNimJson) {
    var smartNimJsonStringify = JSON.stringify(smartNimJson, null, 1)
    var smartNimJsonFilename = "smartNim" + dateStart + "---" + dateEnd + ".json"
    fs.writeFile("./data/" + smartNimJsonFilename, smartNimJsonStringify, function (err) {
        console.log("smartNimJson saved : " + smartNimJsonFilename)
    });
}

// -----------------------------------------------------------------------------------------------------------
// --- Elastic Search  esDslQuerySupplementarySources 
// -----------------------------------------------------------------------------------------------------------
var esDslQueryGetAllSupplementarySources = {
    "from": 0, "size": 10000,
    "sort": [
        {"@timestamp": "asc"}
    ],
    "query": {
        "bool": {
            "must": [
                // {"match": {"_type": "smartnim"}},
                // {"match": {"testType": "ipSla"}},
                {"range": {
                        "@timestamp": {
                            "gte": dateStart, // "2017-09-01T00:00:00",
                            "lt": dateEnd
                        }
                    }
                }
            ]
        }
    }
}

esDslQuerySupplementarySources(esDslQueryGetAllSupplementarySources, function (err, res) {
    if (err) {
        console.log("esDslQuerySupplementarySources ERROR: ", err)
    } else {
        console.log("esDslQuerySupplementarySources OK: ")
        res.map(function (imageObj) {
            decodeSaveImage(imageObj)
        })
    }
})

function esDslQuerySupplementarySources(obj, cbf) {

    var dslQuery = {
        index: properties.supplementarySourcesIndex,
        body: obj
    }

    esClient.search(dslQuery, function (err, res) {
        if (err) {
            cbf(err, null)
        } else {
            cbf(null, res.hits.hits.map(function (d) {
                return d._source
            }))
        }
    });
}

function decodeSaveImage(imageObj) {
    var imageBase64Decoded = new Buffer(imageObj.imageBase64Encoded, 'base64')
    var imageName = imageObj.imageId + "_" + moment(imageObj.timestamp).format('YYYYMMDDTHH:mm:ss')  + ".jpg"
    fs.writeFile("./data/" + imageName, imageBase64Decoded, function (err) {
        console.log("image saved : " + imageName)
    });
}
