Challenge 4. IoT aware intelligent IP network�s behavior prediction

Challenge goal:
- IoT aware intelligent IP network�s behaviour prediction design/architecture/solution.

Outcome:
- Participants have fun time on HackElect event :-) 
- Clear solution concept.
- Open flexible system architecture.
- Innovative approach using as many as possible different sources.
- Innovative flexible data visualization.

Feature List:
- Architecture design (scheme required).
- Number of different sources (e.g. weather, traffic, ... ) identified.
- Number of different sources applied.
- Industry standard technologies/approach/tools used.
- Input data verification methods used.
- Innovative/modern/state of the art backend data processing/analytics/pattern identification..
- Self learning solution introduction into design.
- Number of different data visualization types introduced (e.g. heat map, radar map, ...).
- Visualization flexibility and openness.
- Results presentation.












